import React from 'react';
import FlightPost from './FlightsPost';

const FlightList = ({ flyInfo }) => {

	function fetchFlyInfo(obj) {
		const legs = obj.flight.legs.at(-1)
		const segment = legs.segments.at(-1)
		return segment
	}

	const flyObj = fetchFlyInfo(flyInfo)

	const departureCity = flyObj?.departureCity?.caption || {};
	const departureAirport = flyObj?.departureAirport?.caption || {};
	const departureDate = flyObj?.departureDate || {};
	const arrivalAirport = flyObj?.arrivalAirport?.caption || {};
	const arrivalCity = flyObj?.arrivalCity?.caption || {};
	const arrivalDate = flyObj?.arrivalDate || {};
	const amount = flyInfo?.flight?.price?.total?.amount || {};
	const currency = flyInfo?.flight?.price?.total?.currency || {};
	const travelDuration = flyObj?.travelDuration || {};
	const caption = flyInfo?.flight?.carrier?.caption || {};


	return (
		<div className='posts'>
			<FlightPost
				departureCity={departureCity}
				departureAirport={departureAirport}
				departureDate={departureDate}
				arrivalAirport={arrivalAirport}
				arrivalCity={arrivalCity}
				arrivalDate={arrivalDate}
				amount={amount}
				currency={currency}
				travelDuration={travelDuration}
				caption={caption}
			/>
		</div >
	);
}

export default FlightList;