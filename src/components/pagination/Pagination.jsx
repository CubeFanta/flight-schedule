import React from "react";

const Pagination = ({ pages, pagesArray, page, changePage }) => {
	return (
		<div className="page__wrapper">
			{pages.map(num =>
				<button className={page === num ? 'page page__current' : 'page'}
					onClick={() => changePage(num)}
					key={num}>
					{num}
				</button>
			)}
		</div>
	)
}

export default Pagination;