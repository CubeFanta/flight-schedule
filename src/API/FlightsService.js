import axios from "axios";

export default class FlightServis {
	static async getAll() {
		try {
			const responce = await axios.get("http://localhost:7000/api")
			const { result } = responce.data;

			return result;
		}
		catch (error) {
			console.log(error);
		};
	};
};
