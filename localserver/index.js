const express = require('express')
const fs = require("fs");
const cors = require('cors')

const PORT = process.env.PORT || 7000

const app = express()

app.use(cors())
app.use(express.json())

const jsonParser = express.json();

app.post("/", jsonParser, function (request, response) {
	console.log(request.body);
	if (!request.body) return response.sendStatus(400);

	response.json(request.body);
});

app.get("/api", function (request, response) {

	response.sendFile(__dirname + "/flights.json");
});


app.listen(PORT, () => console.log('server startdet on post ${PORT}'))

