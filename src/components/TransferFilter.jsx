import React, { useState } from "react";

const TransferFilter = () => {
	const [checked, setChecked] = useState(false);

	return (
		<div className="filter__checkbox">
			<h3>Фильтровать</h3>
			<p>
				<input
					type="checkbox"
					defaultChecked={checked}
					onChange={() => setChecked(false)}
				/>
				-одна пересадка
			</p>
			<p>
				<input
					type="checkbox"
					defaultChecked={checked}
					onChange={() => setChecked(false)}
				/>
				-без пересадок
			</p>
		</div>
	)
}

export default TransferFilter;
