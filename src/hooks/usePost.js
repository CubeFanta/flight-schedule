import { useMemo } from "react";

export const useSortedPost = (posts, checkbox, radioSort, price) => {
	const sortedPosts = useMemo(() => {
		if (radioSort) {
			return [...posts].sort((a, b) => {
				const getAmount = item => item?.flight?.price?.total?.amount;
				const getTravelDuration = item => item?.flight.legs.at(-1).segments.at(-1).travelDuration;

				const first = getAmount(a);
				const last = getAmount(b);
				const travelDurationFirst = getTravelDuration(a);
				const travelDurationLast = getTravelDuration(b);

				const result1 = radioSort === 'value1' && first - last;
				const result2 = radioSort === 'value2' && last - first;
				const result3 = radioSort === 'value3' && travelDurationFirst - travelDurationLast;

				return result1 || result2 || result3;
			})
		}
		return posts;
	}, [radioSort, posts])

	return sortedPosts;
}

export const usePricePosts = (posts, checkbox, radioSort, price) => {
	const sortedPost = useSortedPost(posts, checkbox, radioSort, price);
	const filterPricePost = useMemo(() => {
		let result = sortedPost.filter(number => {
			const amount = +number.flight?.price?.total?.amount;
			const max = price.maxPrice || amount;
			const min = price.minPrice || amount;

			return amount >= max && amount <= min;
		})

		return result.length > 0 ? result : sortedPost;
	}, [sortedPost, price])

	return filterPricePost;
}

export const useFilterPosts = (posts, checkbox, radioSort, price) => {
	let filterPricePost = usePricePosts(posts, checkbox, radioSort, price);
	const useFilterPosts = useMemo(() => {
		let result = filterPricePost.filter(elem => {
			const { caption } = elem.flight?.carrier;
			const franch = caption === 'Air France';
			const rus = caption === 'Аэрофлот - российские авиалинии';

			return (checkbox.check1 && franch) || (checkbox.check2 && rus);
		})

		return result.length > 0 ? result : filterPricePost;
	}, [filterPricePost, checkbox])

	return useFilterPosts;
}