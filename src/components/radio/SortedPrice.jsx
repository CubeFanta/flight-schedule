import React, { useState, useMemo } from "react";

const SortedPrice = ({ value, onChange }) => {


	return (
		<div className="sorted-price__radio">
			<h3>Сортировать</h3>
			<p><input
				type="radio"
				value="value1"
				checked={value === 'value1'}
				onChange={event => onChange(event.target.value)}
			/>-По возрастанию
			</p>
			<p>
				<input
					type="radio"
					value="value2"
					checked={value === 'value2'}
					onChange={event => onChange(event.target.value)}
				/>-По убыванию
			</p>
			<p>
				<input
					type="radio"
					value="value3"
					checked={value === 'value3'}
					onChange={event => onChange(event.target.value)}
				/>-По времени
			</p>
		</div >
	)
}

export default SortedPrice;


// input, value, onChange 