import React, { useMemo, useEffect, useState } from "react";
import FlightServis from "./API/FlightsService";
import AirlinesCheckead from "./components/checked/AirlinesCheckead";
import FlightList from "./components/FlightsList";
import Pagination from "./components/pagination/Pagination";
import SortedPrice from "./components/radio/SortedPrice";
import TickedPrice from "./components/TicketPrice";
import TransferFilter from "./components/TransferFilter";
import { useFilterPosts } from "./hooks/usePost";
import './styles/App.css';
import { getPageCount } from "./utils/pages";
import { getPagesArray } from "./utils/pages";
import { createPages } from "./utils/pages";

function App() {
  const [posts, setPosts] = useState([]);
  const [radioSort, setRadioSort] = useState('');
  const [price, setPrice] = useState({
    minPrice: '',
    maxPrice: ''
  });
  const [checkbox, setCheckbox] = useState({
    check1: false,
    check2: false
  });
  const [limit, setLimit] = useState(5);
  const [page, setPage] = useState(1);

  const pages = [];
  const filterNameCompanyPosts = useFilterPosts(posts, checkbox, radioSort, price);
  const totalCount = filterNameCompanyPosts.length;

  let totalPages = getPageCount(totalCount, limit)
  let pagesArray = getPagesArray(totalPages)

  const lastPage = page * limit;
  const firstPage = lastPage - limit;
  const currentPage = filterNameCompanyPosts.slice(firstPage, lastPage)

  createPages(pages, totalPages, currentPage)

  async function fetchPost() {
    const post = await FlightServis.getAll();

    setPosts(post.flights);
  }

  useEffect(() => {
    fetchPost()
  }, []);

  const changePage = (page) => {
    setPage(page)
  }

  return (
    <div className="App">
      <div className="header">
      </div>
      <div className="container">
        <div className="navbar">
          <div className="navbar-radio" id="sorted">
            <SortedPrice
              value={radioSort}
              onChange={setRadioSort}
            />
          </div>
          <div className="navbar-chexbox" id="sorted">
            <TransferFilter />
          </div>
          <div className="navbar-input" id="sorted">
            <TickedPrice
              value={price}
              onChange={setPrice}
            />
          </div>
          <div className="navbar-chexbox" id="sorted">
            <AirlinesCheckead
              value={checkbox}
              onChange={setCheckbox}
            />
          </div>
        </div>
        <div className="content">
          {
            currentPage?.map(flight => <FlightList flyInfo={flight} key={flight.flightToken} />)
          }
          <Pagination
            pagesArray={pagesArray}
            changePage={changePage}
            page={page}
            pages={pages}
          />
        </div>
      </div>
    </div>
  )
}


export default App;
