import React from "react";
import { useState } from "react";

const InputPrice = ({ placeholder, onChange, value }) => {

	return (
		<div>
			<input
				value={value}
				placeholder={placeholder}
				onChange={onChange}
			/>
		</div>
	)
}

export default InputPrice;