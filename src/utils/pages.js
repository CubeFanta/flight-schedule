export const getPageCount = (totalCount, limit) => {
	return Math.ceil(totalCount / limit);
}

export const getPagesArray = (pagesArray) => {
	let result = [];
	for (let i = 0; i < pagesArray; i++) {
		result.push(i + 1);
	}
	return result
}

export function createPages(pages, totalPages, currentPage) {
	if (totalPages > 10) {
		if (currentPage > 5) {
			for (let i = currentPage - 4; i <= currentPage + 5; i++) {
				pages.push(i)
				if (i === totalPages) break
			}
		}
		else {
			for (let i = 1; i <= 10; i++) {
				pages.push(i)
				if (i === totalPages) break
			}
		}
	} else {
		for (let i = 1; i <= totalPages; i++) {
			pages.push(i)
		}
	}
}

// export function createPages(pages, pagesCount, currentPage) {
// 	if (pagesCount > 10) {
// 		if (currentPage > 5) {
// 			for (let i = currentPage - 4; i <= currentPage + 5; i++) {
// 				pages.push(i)
// 				if (i == pagesCount) break
// 			}
// 		}
// 		else {
// 			for (let i = 1; i <= 10; i++) {
// 				pages.push(i)
// 				if (i == pagesCount) break
// 			}
// 		}
// 	} else {
// 		for (let i = 1; i <= pagesCount; i++) {
// 			pages.push(i)
// 		}
// 	}
// }
