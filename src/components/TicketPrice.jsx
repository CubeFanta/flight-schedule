import React, { useMemo, useState } from "react";
import InputPrice from "./input/InputPrice";


const TickedPrice = ({ value, onChange }) => {

	return (
		<div className="priсe">
			<h3>Цена</h3>
			<InputPrice
				name='max'
				placeholder="ot"
				onChange={event => onChange({ ...value, maxPrice: event.target.value })}
				value={value.maxPrice}
			/>
			<br></br>
			<InputPrice
				name='min'
				placeholder="do"
				onChange={event => onChange({ ...value, minPrice: event.target.value })}
				value={value.minPrice}
			/>
		</div>
	)
}

export default TickedPrice;
