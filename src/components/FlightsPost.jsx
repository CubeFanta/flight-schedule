import React from "react";
import '../styles/FlightsPost.css';


const FlightPost = ({
	departureCity,
	departureAirport,
	departureDate,
	arrivalAirport,
	arrivalCity,
	arrivalDate,
	amount,
	currency,
	travelDuration,
	caption
}) => {

	function getTimeFromMins(mins) {
		let hours = Math.trunc(mins / 60);
		let minutes = mins % 60;
		return hours + 'ч' + ' :' + minutes;
	};

	const duration = getTimeFromMins(travelDuration)

	return (
		<div className="post">
			<div className="post_price" id="form">
				<span>Стоимость :{amount},{currency}</span>
			</div>
			<div className="post_location" id="form">
				<span>{departureAirport}{departureCity}</span>
				<span>{arrivalAirport}.{arrivalCity}</span>
			</div>
			<div className="post_data" id="form">
				<span>{departureDate}</span>
				<span>{duration}мин</span>
				<span>{arrivalDate}</span>
			</div>
			<div className="post_operator" id="form">
				<span>Рейс выполняет:</span>
				<span>{caption}</span>
			</div>
		</div>
	);
};

export default FlightPost;
