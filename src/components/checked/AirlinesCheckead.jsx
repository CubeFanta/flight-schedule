import React, { useState, useMemo } from "react";

const AirlinesCheckead = ({ value, onChange }) => {


	return (
		<div className="markAirlines__checked">
			<h3>Авиакомпании</h3>
			<label>
				<p>
					<input
						type="checkbox"
						checked={value.check1}
						onChange={(event) => onChange({ ...value, check1: event.target.checked })}
					/>
					-Air France
				</p>
			</label>
			<label>
				<p>
					<input
						type="checkbox"
						checked={value.check2}
						onChange={(event) => onChange({ ...value, check2: event.target.checked })}
					/>
					-Аэрофлот - российские авиалинии
				</p>
			</label>
		</div>
	)
}

export default AirlinesCheckead;